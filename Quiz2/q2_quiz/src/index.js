import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";

import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green, purple } from '@mui/material/colors';

const theme = createTheme({
  palette: {
   primary: {
      main: '#64b5f6',
      light: '#0d47a1',
      contrastText: 'rgba(239,231,231,0.87)',
    },
    secondary: {
      main: '#ffab40',
    },
  },
});

ReactDOM.render(
 <BrowserRouter>
 <ThemeProvider theme={theme}>
    <App />
</ThemeProvider>
 </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
