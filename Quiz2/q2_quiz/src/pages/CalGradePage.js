import CalGrade from "../components/CalGrade";
import {useState} from "react";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid' ;
import {Typography, Box, container} from '@mui/material';


function CalGradePage() {

    const [name,setName ] = useState("");
    const [calg, setCalGrade] = useState ("0");
    const [translatCalGrade,setTranslateCalGrade ] = useState("");

    const [score ,setScore] = useState("");


function calculateGRADE(){
    let s = parseInt (score);
    let calg = s;
    setCalGrade (calg) ;
    if(calg < 50) {
        setTranslateCalGrade  ("E")   
    }if(calg >= 50 && calg< 60) { 
        setTranslateCalGrade ("D")   
    }if(calg >= 60 && calg < 70) { 
        setTranslateCalGrade ("C")   
    }if(calg >= 70 && calg < 80) { 
        setTranslateCalGrade ("B")   
    }if(calg >= 80) { 
        setTranslateCalGrade ("A")   
    }
}


    return(
        <container MaxWidth ='lg'>
        <Grid container spacing={2} sx={{ marginTop : "10px" }}>
             <Grid item xs={12}>
                <Typography variant ="h5">
                ยินดีต้อนรับเข้าสู่เว็บคำนวณเกรด
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Box xs={{ textAlign : 'center'}}>
                คุณชื่อ : <input type="text" 
                            value={name} 
                            onChange ={ (e) => { setName(e.target.value);} } /> 
                            <br />
                            <br />
                คะเเนนที่ได้ : <input type="text" 
                            value={score} 
                            onChange ={ (e) => { setScore(e.target.value);} } /> 
                            <br />
                            <br />
                
                <Button variant="contained" onClick = { () => { calculateGRADE () } } > 
                        CALCULATE
                </Button>
                </Box>
            </Grid>

            <Grid item xs={12}>
            <Box xs={{ textAlign : 'center'}}>
                { calg != 0 &&
                    <div>
                        <hr /> 
                        ผลคำนวณ
                        <CalGrade 
                            name = { name }
                            score = { score }
                            grade = { translatCalGrade }
                        />
                    </div>   
                }
            </Box>
            </Grid>
           
        </Grid>
        </container>
    );

}
export default CalGradePage;

/*<div>
            <div  align= "center">
              มาคำนวณเกรดกันเถอะ
              <hr />

              คุณชื่อ : <input type="text" 
                        value={name} 
                        onChange ={ (e) => { setName(e.target.value);} } /> <br />
              คะเเนนที่ได้ : <input type="text" 
                        value={score} 
                        onChange ={ (e) => { setScore(e.target.value);} } /> <br />
             
              <Button variant="contained" onClick = { () => { calculateGRADE () } } > 
                       CALCULATE
              </Button>

            { calg != 0 &&
                <div>
                    <hr /> 
                    ผลคำนวณ
                    <CalGrade 
                        name = { name }
                        score = { score }
                        grade = { translatCalGrade }
                    />
                </div>   
             }
            </div>
        </div>*/