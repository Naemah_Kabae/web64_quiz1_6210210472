import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { width } from '@mui/system';

function AboutMe (props) {

    return (
        <Box sx= {{ width:"60%"}}>
        <Paper elevation={3}>
      
            <h2>จัดทำโดย : { props.name }</h2>
            <h2>รหัสนักศึกษา 6210210472</h2>
            <h2>ภาควิชาวิทยาการคอมพิวเตอร์</h2>
        </Paper>
        </Box>
    );
}

export default AboutMe;