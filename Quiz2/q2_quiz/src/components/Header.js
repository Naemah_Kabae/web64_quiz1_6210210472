import { Link } from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header () {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
             <Typography variant="h6"> 
                 ยินดีต้อนรับเข้าสู่เว็บคำนวณเกรด  : 
            </Typography>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <Link to="/grade">
            <Typography variant="body1"> 
                คำนวณเกรด 
            </Typography>
            </Link>   
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <Link to="/about">
            <Typography variant="body1"> 
                ผู้จัดทำ 
            </Typography>
            </Link> 
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                     
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;

/*function Header() {

    return(
        <div align="left">
            ยินดีต้อนรับ : 
                    <Link to="/about">ผู้จัดทำ </Link> 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to="/grade">คำนวนเกรด </Link> 

            <hr/>
        </div>

    );
}

export default Header;*/