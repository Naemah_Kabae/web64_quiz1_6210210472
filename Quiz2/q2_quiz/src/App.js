import logo from './logo.svg';
import './App.css';

import AboutMePage  from './pages/AboutMePage'; 
import Header from './components/Header';
import CalGredePage from './pages/CalGradePage';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className ="App">
      <Header />

      <Routes>
         <Route path="about" element={
               <AboutMePage/>
           } />
      </Routes>

      <Routes>
         <Route path="grade" element={
               <CalGredePage/>
           } />
      </Routes>


    </div>
  );
}

export default App;
